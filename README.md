### Simple usage is:
```bash
python3 ./example.py --url https://demo.voicelab.ai/classify --token PASS --audio-file AUDIO_FILE --conf-name CONF_NAME --pid PID --content-type 'audio/l16;rate=16000'
```
### Options with timestamps:
```bash
python3 ./example.py --url https://demo.voicelab.ai/classify --token PASS --audio-file AUDIO_FILE --conf-name CONF_NAME --pid PID --content-type 'audio/l16;rate=16000' --timestamps
```
